﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BobScript : MonoBehaviour
{
    public Color defaultMaterialColor;
    public Color highlightedColor;
    public Color selectedColor;

    public Material bobMaterial;

    public Text timerText;
    public GameObject timerCanvas;

    public Transform pendulumTransform;

    public Camera cam;
    
    private void Awake()
    {
        defaultMaterialColor = bobMaterial.color;
        layerMask = LayerMask.GetMask("bobLayer");
        quadMask = LayerMask.GetMask("quadLayer");
        timerCanvas.SetActive(false);
    }
    RaycastHit hit;
    Ray ray;
    int layerMask;
    int quadMask;

    bool isHighlighted;
    bool isSelected;

    private void FixedUpdate()
    {
        ray = cam.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, 5.0f, layerMask))
        {
            if (!isHighlighted)
            {
                isHighlighted = true;
                bobMaterial.color = highlightedColor;
            }
            if (Input.GetMouseButton(0))
            {
                timerCanvas.SetActive(false);
                StopAllCoroutines();
                if (!isSelected)
                {
                    isSelected = true;
                    bobMaterial.color = selectedColor;
                }

                if (Physics.Raycast(ray, out hit, 5.0f, quadMask))
                {
                    Vector3 globalPoint = hit.point;
                    globalPoint.x = 0.0f;
                    pendulumTransform.LookAt(globalPoint);
                }
            }
            else
            {
                if (Input.GetMouseButtonUp(0))
                {
                    timerCanvas.SetActive(true);
                    StartCoroutine(TimerInSeconds());
                }
                isSelected = false;
                bobMaterial.color = highlightedColor;
            }
        }
        else
        {
            isHighlighted = false;
            bobMaterial.color = defaultMaterialColor;
        }
    }

    float time;
    IEnumerator TimerInSeconds()
    {
        time = 0.0f;

        while (true)
        {
            time += Time.deltaTime;
            int minute = Mathf.FloorToInt(time / 60f);
            float seconds = time % 60f;
            timerText.text = string.Format("{0:00}:{1:00.00}", minute, seconds);
            yield return null;
        }
    }

    public void ResetTimer()
    {
        StopAllCoroutines();
        timerText.text = "00:00.00";
    }
}
