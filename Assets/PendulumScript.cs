﻿using UnityEngine;

public class PendulumScript : MonoBehaviour
{
    Transform pendulumTransform;
    public Transform bobTransform;

    float defaultXScale;

    private void Awake()
    {
        pendulumTransform = GetComponent<Transform>();
        defaultXScale = pendulumTransform.localScale.z;
    }

    public void AdjustLength(float newLength)
    {
        Vector3 scale = pendulumTransform.localScale;
        pendulumTransform.localScale = new Vector3(scale.x, scale.y, newLength);
        scale = bobTransform.localScale;
        bobTransform.localScale = new Vector3(scale.x, scale.y, defaultXScale * scale.z / newLength);
    }
}
